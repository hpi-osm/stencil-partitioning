#include "Utils.h"

#include <limits>

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Utils
//
////////////////////////////////////////////////////////////////////////////////////////////////////

double edgeCosts(const Point &p1, const Point &p2)
{
	const auto dx = std::abs(p1.x() - p2.x());
	const auto dy = std::abs(p1.y() - p2.y());

	return std::max(dx, dy);
	//return boost::geometry::length(segment);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Line::isInFrame() const
{
	const auto angleToNearestDiagonal = std::abs(angle - M_PI / 2.0 * std::floor(angle / M_PI * 2.0) - M_PI / 4.0);
	const auto projectionToDiagonal = distance / std::cos(angleToNearestDiagonal);

	return projectionToDiagonal <= std::sqrt(2.0);
}
