#ifndef __MODEL_H
#define __MODEL_H

#include <iosfwd>
#include <boost/geometry.hpp>
#include <boost/filesystem.hpp>
#include <cairomm/cairomm.h>

#include "Utils.h"
#include "Polygon.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Model
//
////////////////////////////////////////////////////////////////////////////////////////////////////

class Model
{
	public:
		Model(size_t numberOfLines, Polygon outline);
		Model randomize(double angleRange, double distanceRange, bool onlyDiagonals) const;

		void generateRandom(bool onlyDiagonals);

		void draw(const boost::filesystem::path &path) const;
		void save(const boost::filesystem::path &path, size_t numberOfLabels) const;

		const std::vector<Segment> &segments() const;
		const std::vector<Intersection> &intersections() const;
		const std::vector<Polygon> &polygons() const;

		const Polygon &biggestPolygon() const;

		void computeSegments();
		void computeIntersections();
		void computePolygons();
		void mergePolygons(size_t numberOfLabels);

		bool isSolution() const;
		double costs() const;

		double score() const;

	private:
		Model() = delete;

		size_t m_numberOfLines;

		// Seed
		std::vector<Line> m_lines;

		// Cache
		Polygon m_outline;
		std::vector<Segment> m_segments;

		std::vector<Point> m_points;
		std::vector<Polygon> m_polygons;

		std::vector<Intersection> m_intersections;
		std::vector<IntersectionsOnSegment> m_intersectionsOnSegments;

		std::vector<PolygonAdjacency> m_polygonAdjacencies;

		double m_costs;
		double m_score;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

std::ostream &operator<<(std::ostream &ostream, const Model &model);

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif
