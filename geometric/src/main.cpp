#include <iostream>

#include <memory>
#include <thread>
#include <mutex>

#include "Model.h"

std::mutex counterMutex;
size_t counter = 0;

const Polygon squareOutline = Polygon(
	{
		Point(-1.0, -1.0),
		Point(1.0, -1.0),
		Point(1.0, 1.0),
		Point(-1.0, 1.0)
	});

const Polygon triangleOutline = Polygon(
	{
		Point(-1.0, 0.866),
		Point(1.0, 0.866),
		Point(0.0, -0.866)
	});

const Polygon cornerTriangleOutline = Polygon(
	{
		Point(-1.0, -1.0),
		Point(-1.0, 1.0),
		Point(1.0, -1.0)
	});

const Polygon cornerOutline = Polygon(
	{
		Point(-1.0, 1.0),
		Point(1.0 - std::sqrt(2.0), 1.0),
		Point(1.0 - std::sqrt(2.0) / 2.0, 1.0 - std::sqrt(2.0) / 2.0),
		Point(-1.0 + std::sqrt(2.0) / 2.0, -1.0 + std::sqrt(2.0) / 2.0),
		Point(-1.0, -1.0 + std::sqrt(2.0))
	});

bool testModel(Model &model, const double &bestScore, const size_t numberOfLabels)
{
	/*{
		std::lock_guard<std::mutex> lock(counterMutex);
		counter++;

		if (counter % 1000 == 0)
			std::cout << "    " << counter << std::endl;
	}*/

	model.computeSegments();
	model.computeIntersections();
	model.computePolygons();

	if (model.biggestPolygon().area() > 1.5 * 4.0 / numberOfLabels)
		return false;

	model.mergePolygons(numberOfLabels);

	if (!model.isSolution())
		return false;

	if (model.score() >= bestScore)
		return false;

	return true;
}

void writeModel(const Model &model, const std::string &suffix, double &bestScore, std::mutex &bestModelMutex, size_t numberOfLabels)
{
	std::lock_guard<std::mutex> lock(bestModelMutex);
	const auto costs = model.costs();
	const auto score = model.score();

	std::cout << score << suffix << std::endl;

	/*auto test = model.randomize(0.0);
	test.computeSegments();
	test.computeIntersections();
	test.computePolygons();
	test.mergePolygons(numberOfLabels, 2.0);

	std::cout << test.score() << suffix << std::endl;*/

	bestScore = score;

	const auto path = "/tmp/model_" + std::to_string(numberOfLabels) + "_s" + std::to_string(score) + "_c" + std::to_string(costs) + suffix;
	model.draw(path + ".pdf");
	model.save(path + ".csv", numberOfLabels);
}

int main(int argc, char *argv[])
{
	const auto onlyDiagonals = true;

	if (argc < 3)
		exit(EXIT_FAILURE);

	const size_t numberOfLabels = std::atoi(argv[1]);
	const size_t numberOfLines = std::atoi(argv[2]);

	std::mutex bestModelMutex;
	auto bestScore = std::numeric_limits<double>::max();

	std::vector<std::unique_ptr<std::thread>> threads;
	std::vector<std::unique_ptr<std::thread>> localSearchThreads;

	const size_t numberOfThreads = 9;

	for (size_t i = 0; i < numberOfThreads; i++)
		threads.push_back(std::make_unique<std::thread>(
			[&]()
			{
				Model model(numberOfLines, squareOutline);

				// <DEBUG OMG1>
				//model.generateRandom(onlyDiagonals);
				//model.computeSegments();
				//model.computeIntersections();
				//model.generateRandom(onlyDiagonals);
				//model.generateRandom(onlyDiagonals);
				// </DEBUG OMG1>

				while (true)
				{
					model.generateRandom(onlyDiagonals);

					if (!testModel(model, bestScore, numberOfLabels))
						continue;

					writeModel(model, "", bestScore, bestModelMutex, numberOfLabels);

					// Local search on best models
					localSearchThreads.push_back(std::make_unique<std::thread>(
						[model, &bestModelMutex, numberOfLabels]()
						{
							Model localSearchBestModel = model;
							auto localSearchBestScore = model.score();

							const size_t numberOfIterations = 1000;

							for (size_t i = 0; i < numberOfIterations; i++)
							{
								const auto temperature = std::pow(1.0 - static_cast<double>(i) / numberOfIterations, 2.0);

								const double angleRange = temperature * 10.0 / M_PI / 2.0;
								const double distanceRange = temperature * 0.25;

								Model localSearchModel = localSearchBestModel.randomize(angleRange, distanceRange, onlyDiagonals);

								if (!testModel(localSearchModel, localSearchBestScore, numberOfLabels))
									continue;

								const auto oldScore = localSearchBestScore;

								localSearchBestModel = localSearchModel;
								localSearchBestScore = localSearchModel.score();

								writeModel(localSearchBestModel, "_l_" + std::to_string(oldScore), localSearchBestScore, bestModelMutex, numberOfLabels);
							}

							std::cout << "local search finished" << std::endl;
						}));
				}
			}));

	for (size_t i = 0; i < numberOfThreads; i++)
		threads[i]->join();

	//std::cout << model;

	return 0;
}
