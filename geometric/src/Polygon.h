#ifndef __POLYGON_H
#define __POLYGON_H

#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Polygon
//
////////////////////////////////////////////////////////////////////////////////////////////////////

class Polygon
{
	public:
		using Geometry = boost::geometry::model::polygon<Point::Geometry>;

	public:
		Polygon();
		Polygon(Geometry geometry);
		Polygon(std::vector<Point> points);

		void setGeometry(Geometry geometry);
		const Geometry &geometry() const;

		size_t numberOfPoints() const;

		double area() const;
		double costs() const;

		void setLabel(int label);
		int label() const;

		bool isValid() const;

	private:
		static constexpr double InvalidArea = -1.0;
		static constexpr double InvalidCosts = -1.0;

	private:
		Geometry m_geometry;
		mutable double m_area;
		mutable double m_costs;
		int m_label;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

struct PolygonAdjacency
{
	Polygon *polygon1;
	size_t polygon1Index;
	Polygon *polygon2;
	size_t polygon2Index;
	const Point *p1;
	const Point *p2;
	//double edgeCosts;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

bool operator==(const Polygon &lhs, const Polygon &rhs);

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif
