#include "Model.h"

#include <random>
#include <iostream>
#include <fstream>

#include <boost/assert.hpp>
#include <boost/dynamic_bitset.hpp>

#include "Timer.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Model
//
////////////////////////////////////////////////////////////////////////////////////////////////////

Model::Model(size_t numberOfLines, Polygon outline)
:	m_numberOfLines{numberOfLines},
	m_outline(outline),
	m_costs{std::numeric_limits<double>::max()},
	m_score{std::numeric_limits<double>::max()}
{
	// Count borders
	const auto numberOfSegments = numberOfLines + m_outline.numberOfPoints();

	const size_t upperBoundOfIntersections = std::pow(std::ceil(numberOfSegments / 2.0), 2);
	const size_t upperBoundOfIntersectionsOnSegment = numberOfSegments - 1;
	const size_t upperBoundOfPolygons = std::pow(std::ceil(numberOfSegments / 2.0) + 1, 2);
	const size_t upperBoundOfPolygonAdjacency = 2 * upperBoundOfPolygons;

	m_lines.reserve(numberOfLines);
	m_segments.reserve(numberOfSegments);
	m_points.reserve(upperBoundOfIntersections);
	m_polygons.reserve(upperBoundOfPolygons);
	m_intersections.reserve(upperBoundOfIntersections);
	m_intersectionsOnSegments.reserve(upperBoundOfIntersectionsOnSegment);
	m_polygonAdjacencies.reserve(upperBoundOfPolygonAdjacency);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Model Model::randomize(double angleRange, double distanceRange, bool onlyDiagonals) const
{
	Model model(m_numberOfLines, m_outline);
	model.m_lines = m_lines;

	std::for_each(model.m_lines.begin(), model.m_lines.end(),
		[&](auto &line)
		{
			std::random_device randomDevice;
			std::mt19937 randomGenerator(randomDevice());
			std::uniform_real_distribution<> randomDistribution(-1.0, 1.0);

			do
			{
				if (!onlyDiagonals)
					line.angle = line.angle + randomDistribution(randomGenerator) * angleRange;

				line.distance = line.distance + randomDistribution(randomGenerator) * distanceRange;
			}
			while (!line.isInFrame());
		});

	return model;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Model::generateRandom(bool onlyDiagonals)
{
	m_lines.clear();
	m_segments.clear();
	m_points.clear();
	m_polygons.clear();
	m_intersections.clear();
	m_intersectionsOnSegments.clear();
	m_polygonAdjacencies.clear();

	BOOST_ASSERT(m_segments.capacity() == m_numberOfLines + m_outline.numberOfPoints());

	std::random_device randomDevice;
	std::mt19937 randomGenerator(randomDevice());
	std::uniform_real_distribution<> randomDistribution(0.0, 1.0);

	while (m_lines.size() < m_numberOfLines)
	{
		while (true)
		{
			double angle;

			if (onlyDiagonals)
				angle = M_PI / 4.0 + M_PI / 2.0 * std::floor(randomDistribution(randomGenerator) * 4.0);
			else
				angle = 2.0 * M_PI * randomDistribution(randomGenerator);

			const auto distance = std::sqrt(2.0) * randomDistribution(randomGenerator);
			const Line line = {angle, distance};

			if (!line.isInFrame())
				continue;

			m_lines.push_back(line);
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Model::computeSegments()
{
	m_segments.clear();

	for (size_t i = 0; i < m_outline.geometry().outer().size() - 1; i++)
	{
		const auto &p1 = m_outline.geometry().outer()[i];
		const auto &p2 = m_outline.geometry().outer()[i + 1];

		m_segments.push_back(Segment(Point(p1), Point(p2)));
	}

	std::for_each(m_lines.cbegin(), m_lines.cend(),
		[&](const auto &line)
		{
			const auto normalX = std::cos(line.angle);
			const auto normalY = std::sin(line.angle);

			const auto x = line.distance * normalX;
			const auto y = line.distance * normalY;

			const Point p1(x - 10 * normalY, y + 10 * normalX);
			const Point p2(x + 10 * normalY, y - 10 * normalX);

			m_segments.push_back(Segment(p1, p2));
		});
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Model::computeIntersections()
{
	m_intersections.clear();
	m_intersectionsOnSegments.clear();

	auto findIntersectionsOnSegment =
		[&](const auto &segment) -> IntersectionsOnSegment &
		{
			const auto match = std::find_if(m_intersectionsOnSegments.begin(), m_intersectionsOnSegments.end(),
				[&](const auto &intersectionsOnSegment)
				{
					return intersectionsOnSegment.segment == &segment;
				});

			if (match != m_intersectionsOnSegments.end())
				return *match;

			m_intersectionsOnSegments.push_back({&segment, std::vector<const Point *>()});

			return m_intersectionsOnSegments.back();
		};

	for (auto segment1 = m_segments.cbegin(); segment1 != m_segments.cend(); segment1++)
		for (auto segment2 = segment1 + 1; segment2 != m_segments.cend(); segment2++)
		{
			std::vector<Point::Geometry> intersections;
			boost::geometry::intersection((*segment1).geometry(), (*segment2).geometry(), intersections);

			if (intersections.empty())
				continue;

			BOOST_ASSERT_MSG(intersections.size() == 1, "Lines overlap, unhandled yet");

			m_points.push_back(intersections[0]);
			const auto &intersection = m_points.back();

			// within square [-1, +1] x [-1, +1]
			//if (!intersection.isInFrame())
			//	continue;

			if (!boost::geometry::covered_by(intersection.geometry(), m_outline.geometry()))
				continue;

			m_intersections.push_back({&*segment1, &*segment2, &intersection});

			auto &intersectionsOnSegment1 = findIntersectionsOnSegment(*segment1);
			intersectionsOnSegment1.points.push_back(&intersection);
			auto &intersectionsOnSegment2 = findIntersectionsOnSegment(*segment2);
			intersectionsOnSegment2.points.push_back(&intersection);
		}
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Model::computePolygons()
{
	auto segmentsForIntersection =
		[&](const Point *p)
		{
			const auto match = std::find_if(m_intersections.cbegin(), m_intersections.cend(),
				[&](const auto &intersection)
				{
					return intersection.point == p;
				});

			BOOST_ASSERT(match != m_intersections.cend());

			return std::make_tuple((*match).segment1, (*match).segment2);
		};

	auto onRightSide =
		[&](const Segment &segment, const Point &p)
		{
			const auto &p1 = segment.p1();
			const auto &p2 = segment.p2();

			return ((p2.x() - p1.x()) * (p.y() - p1.y()) - (p2.y() - p1.y()) * (p.x() - p1.x())) > 0;
		};

	auto nearestRightNeighbor =
		[&](const Segment &segmentPrev, const Segment *segment, const Point *p)
		{
			const Point *result = nullptr;
			double bestDistance = std::numeric_limits<double>::max();

			const auto intersectionsOnSegment = std::find_if(m_intersectionsOnSegments.cbegin(), m_intersectionsOnSegments.cend(),
				[&](const auto &intersectionsOnSegment)
				{
					return intersectionsOnSegment.segment == segment;
				});

			BOOST_ASSERT(intersectionsOnSegment != m_intersectionsOnSegments.cend());

			std::for_each((*intersectionsOnSegment).points.cbegin(), (*intersectionsOnSegment).points.cend(),
				[&](const Point *intersection)
				{
					if (intersection == p)
						return;

					if (onRightSide(segmentPrev, *intersection))
						return;

					const double distance = boost::geometry::distance(p->geometry(), intersection->geometry());

					if (distance >= bestDistance)
						return;

					bestDistance = distance;
					result = intersection;
				});

			return result;
		};

	auto adjacentPolygon =
		[&](const Point *p1, const Point *p2)
		{
			std::vector<const Point *> points;
			points.push_back(p1);
			points.push_back(p2);

			while (true)
			{
				const auto *p1 = points[points.size() - 2];
				const auto *p2 = points[points.size() - 1];
				const auto segments1 = segmentsForIntersection(p1);
				const auto segments2 = segmentsForIntersection(p2);
				const auto segmentPrev = Segment(*p1, *p2);

				const Point *pn = nullptr;

				if (std::get<0>(segments1) == std::get<0>(segments2)
					|| std::get<1>(segments1) == std::get<0>(segments2))
				{
					pn = nearestRightNeighbor(segmentPrev, std::get<1>(segments2), p2);
				}
				else
					pn = nearestRightNeighbor(segmentPrev, std::get<0>(segments2), p2);

				if (pn == points[0])
					break;

				if (!pn)
					break;

				points.push_back(pn);
			}

			Polygon::Geometry result;
			std::for_each(points.cbegin(), points.cend(),
				[&](const Point *p)
				{
					boost::geometry::append(result, p->geometry());
				});
			boost::geometry::append(result, points.front()->geometry());

			return Polygon(result);
		};

	std::for_each(m_intersectionsOnSegments.begin(), m_intersectionsOnSegments.end(),
		[&](auto &intersectionsOnSegment)
		{
			std::sort(intersectionsOnSegment.points.begin(), intersectionsOnSegment.points.end(),
				[&](const Point *p1, const Point *p2)
				{
					const Point &p0 = intersectionsOnSegment.segment->p1();

					return boost::geometry::distance(p1->geometry(), p0.geometry()) < boost::geometry::distance(p2->geometry(), p0.geometry());
				});

			const auto &intersections = intersectionsOnSegment.points;

			for (size_t i = 0; i < intersections.size() - 1; i++)
			{
				const Point *p1 = intersections[i];
				const Point *p2 = intersections[i + 1];

				const auto polygon1 = adjacentPolygon(p1, p2);
				const auto polygon2 = adjacentPolygon(p2, p1);

				auto match1 = std::find(m_polygons.begin(), m_polygons.end(), polygon1);
				auto match2 = std::find(m_polygons.begin(), m_polygons.end(), polygon2);

				if (polygon1.isValid() && match1 == m_polygons.end())
				{
					m_polygons.push_back(polygon1);
					match1 = std::prev(m_polygons.end());
				}

				if (polygon2.isValid() && match2 == m_polygons.end())
				{
					m_polygons.push_back(polygon2);
					match2 = std::prev(m_polygons.end());
				}

				if (polygon1.isValid() && polygon2.isValid())
				{
					BOOST_ASSERT(match1 != m_polygons.end());
					BOOST_ASSERT(match2 != m_polygons.end());

					/*Point::Geometry c1, c2;
					boost::geometry::centroid(polygon1.geometry(), c1);
					boost::geometry::centroid(polygon2.geometry(), c2);
					std::cout << Point(c1) << "—" << Point(c2) << std::endl;*/

					m_polygonAdjacencies.push_back(
						{&*match1, static_cast<size_t>(match1 - m_polygons.begin()),
							&*match2, static_cast<size_t>(match2 - m_polygons.begin()), p1, p2});
				}
			}
		});
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Model::mergePolygons(size_t numberOfLabels)
{
	auto computeTotalCosts =
		[&](const auto &merges)
		{
			double costs = 0.0;

			// Base costs of each polygon
			std::for_each(m_polygons.cbegin(), m_polygons.cend(),
				[&](const auto &polygon)
				{
					costs += polygon.costs();
				});

			for (size_t i = 0; i < merges.size(); i++)
			{
				if (!merges[i])
					continue;

				costs -= 2 * edgeCosts(*m_polygonAdjacencies[i].p1, *m_polygonAdjacencies[i].p2);
			}

			// Each border edge has a cost of 2.0, which doesn't count
			return (costs - m_outline.costs()) / 2.0;
		};

	m_costs = std::numeric_limits<double>::max();
	m_score = std::numeric_limits<double>::max();

	boost::dynamic_bitset<> currentMerges(m_polygonAdjacencies.size());
	std::vector<int> currentLabels(m_polygons.size(), 0);
	std::vector<double> currentAreas(m_polygons.size(), 0);

	auto increment =
		[](boost::dynamic_bitset<> &bitset)
		{
			for (int loop = 0; loop < bitset.size(); ++loop)
				if (bitset[loop].flip() == 1)
					break;

			return bitset;
		};

	do
	{
		for (int i = 0; i < m_polygons.size(); i++)
			currentLabels[i] = numberOfLabels + i;

		size_t remainingNumberOfLabels = m_polygons.size();

		for (size_t i = 0; i < m_polygonAdjacencies.size(); i++)
		{
			if (!currentMerges[i])
				continue;

			if (currentLabels[m_polygonAdjacencies[i].polygon1Index] == currentLabels[m_polygonAdjacencies[i].polygon2Index])
				continue;

			remainingNumberOfLabels--;

			const auto minLabel = std::min(currentLabels[m_polygonAdjacencies[i].polygon1Index], currentLabels[m_polygonAdjacencies[i].polygon2Index]);
			const auto maxLabel = std::max(currentLabels[m_polygonAdjacencies[i].polygon1Index], currentLabels[m_polygonAdjacencies[i].polygon2Index]);

			for (auto &label : currentLabels)
				if (label == maxLabel)
					label = minLabel;
		}

		if (remainingNumberOfLabels != numberOfLabels)
			continue;

		double minArea = std::numeric_limits<double>::max();
		double maxArea = 0.0;

		for (int label = numberOfLabels; label < numberOfLabels + m_polygons.size(); label++)
		{
			double area = 0.0;

			for (size_t i = 0; i < m_polygons.size(); i++)
			{
				if (currentLabels[i] != label)
					continue;

				area += m_polygons[i].area();
			}

			if (area == 0.0)
				continue;

			minArea = std::min(minArea, area);
			maxArea = std::max(maxArea, area);
		}

		const auto costs = computeTotalCosts(currentMerges);
		//const auto score = (maxArea / minArea < maxAreaError) ? costs : std::numeric_limits<double>::max();
		const auto score = costs * (maxArea / minArea);

		if (score >= m_score)
			continue;

		m_costs = costs;
		m_score = score;

		auto transferLabel =
			[&](auto oldLabel, auto newLabel)
			{
				for (size_t i = 0; i < m_polygons.size(); i++)
				{
					if (currentLabels[i] != oldLabel)
						continue;

					m_polygons[i].setLabel(newLabel);
					currentLabels[i] = std::numeric_limits<int>::max();
				}
			};

		size_t i = 0;

		while (true)
		{
			const auto minLabel = std::accumulate(currentLabels.cbegin(), currentLabels.cend(), std::numeric_limits<int>::max(),
				[&](int minLabel, const auto &currentLabel)
				{
					return std::min(minLabel, currentLabel);
				});

			if (minLabel == std::numeric_limits<int>::max())
				break;

			transferLabel(minLabel, i);
			i++;
		}
	}
	while (increment(currentMerges).any());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

const Polygon &Model::biggestPolygon() const
{
	if (m_polygons.empty())
	{
		std::for_each(m_lines.cbegin(), m_lines.cend(),
			[&](const auto &line)
			{
				std::cout << "phi = " << line.angle / M_PI * 180.0 << "°\td = " << line.distance << std::endl;
			});
		draw("/tmp/fail.pdf");
	}

	BOOST_ASSERT(!m_polygons.empty());

	const auto *result = std::accumulate(m_polygons.begin(), m_polygons.end(), (const Polygon *)(nullptr),
		[&](const auto *biggestPolygon, const auto &polygon)
		{
			if (biggestPolygon == nullptr || polygon.area() > biggestPolygon->area())
				return &polygon;

			return biggestPolygon;
		});

	return *result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Model::draw(const boost::filesystem::path &path) const
{
	const double pageWidth = 200;
	const double pageHeight = 200;
	const double width = 100;
	const double height = 100;
	const auto borderLeft = (pageWidth - width) / 2.0;
	const auto borderTop = (pageHeight - height) / 2.0;

	auto surface = Cairo::PdfSurface::create(path.string(), pageWidth, pageHeight);
	auto context = Cairo::Context::create(surface);

	auto map = [&](const Point &point)
	{
		return Point(
			borderLeft + (point.x() + 1.0) / 2.0 * width,
			borderTop + (point.y() + 1.0) / 2.0 * height);
	};

	const std::vector<std::array<short, 3>> colors =
		{
			{{252, 233, 79}},
			{{138, 226, 52}},
			{{252, 175, 62}},
			{{114, 159, 207}},
			{{173, 127, 168}},
			{{239, 41, 41}},
			{{0, 237, 116}},
			{{210, 22, 146}},
			{{174, 245, 0}},
			{{32, 74, 135}},
			{{38, 135, 38}},
			{{160, 160, 160}},
		};

	for (size_t i = 0; i < m_polygons.size(); i++)
	{
		const Polygon &polygon = m_polygons[i];
		const auto color = colors[polygon.label() % colors.size()];
		const Point p0 = map(polygon.geometry().outer()[0]);

		context->save();
		context->set_source_rgb(std::get<0>(color) / 255.0, std::get<1>(color) / 255.0, std::get<2>(color) / 255.0);
		context->move_to(p0.x(), p0.y());

		std::for_each(polygon.geometry().outer().cbegin(), polygon.geometry().outer().cend(),
			[&](const Point &point)
			{
				const Point p = map(point);
				context->line_to(p.x(), p.y());
			});

		context->line_to(p0.x(), p0.y());

		context->fill();
		context->restore();
	}

	std::for_each(m_segments.cbegin(), m_segments.cend(),
		[&](const Segment &segment)
		{
			const Point p1 = map(segment.p1());
			const Point p2 = map(segment.p2());

			context->save();
			context->set_line_width(0.5);
			context->set_source_rgba(0.0, 0.0, 0.0, 0.25);
			context->move_to(p1.x(), p1.y());
			context->line_to(p2.x(), p2.y());
			context->stroke();
			context->restore();
		});

	context->save();
	context->set_line_width(0.5);
	context->set_source_rgb(0.0, 0.0, 0.0);
	context->rectangle(borderLeft, borderTop, width, height);
	context->stroke();
	context->restore();

	std::for_each(m_intersections.cbegin(), m_intersections.cend(),
		[&](const auto &intersection)
		{
			const Point p = map(*intersection.point);

			context->save();
			context->set_source_rgba(0.0, 0.0, 0.0, 1.0);
			context->arc(p.x(), p.y(), 1.0, 0, 2.0 * M_PI);
			context->fill();
			context->restore();
		});

	context->show_page();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Model::save(const boost::filesystem::path &path, size_t numberOfLabels) const
{
	std::ofstream csv;
	csv.open(path.string());

	for (size_t label = 0; label < numberOfLabels; label++)
	{
		boost::geometry::model::multi_polygon<Polygon::Geometry> polygon;

		for_each(m_polygons.cbegin(), m_polygons.cend(),
			[&](const Polygon &otherPolygon)
		 	{
				if (otherPolygon.label() != label)
					return;

				decltype(polygon) out;

				boost::geometry::union_(polygon, otherPolygon.geometry(), out);

				polygon = out;
		 	});

		assert(polygon.size() == 1);

	 	std::for_each(polygon[0].outer().cbegin(), polygon[0].outer().cend(),
			[&](const Point &point)
			{
				csv << point.x() << "\t" << point.y() << std::endl;
			});

		csv << std::endl;
	}

	csv.close();
}

////////////////////////////////////////////////////////////////////////////////////////////////////1

const std::vector<Segment> &Model::segments() const
{
	return m_segments;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

const std::vector<Intersection> &Model::intersections() const
{
	return m_intersections;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

const std::vector<Polygon> &Model::polygons() const
{
	return m_polygons;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Model::isSolution() const
{
	return m_polygons[0].label() >= 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Model::costs() const
{
	return m_costs;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Model::score() const
{
	return m_score;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

std::ostream &operator<<(std::ostream &ostream, const Model &model)
{
	std::for_each(model.segments().cbegin(), model.segments().cend(),
		[&](const auto &segment)
		{
			ostream << segment.p1() << "–" << segment.p2() << " ";
		});

	return ostream;
}
