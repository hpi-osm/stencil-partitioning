#pragma once

#include "ProcessingUnit.h"

using PowerRatios = std::vector<float>;

PowerRatios makeEqualPowerRatios(const size_t &count)
{
	return PowerRatios(count, 1.0f / count);
}

using Costs = std::vector<Cost>;

Costs makeCosts(const size_t &count)
{
	return Costs(count, Cost(count, 1.0f));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Data>
class ProcessingUnits
{
	// Methods

	public:
		template<typename Function>
		ProcessingUnits(Function function, const PowerRatios &powerRatios, const Costs &costs)
		:	m_communicationDirty(true)
		{
			m_processingUnits.reserve(powerRatios.size());

			for (size_t id = 0; id < powerRatios.size(); ++id)
				m_processingUnits.emplace_back(id, function, powerRatios.at(id), costs.at(id),
					m_allocation);
		}

		size_t numberOfProcessingUnits() const
		{
			return m_processingUnits.size();
		}

		Allocation<Data> & allocation()
		{
			return m_allocation;
		}

		const Allocation<Data> & allocation() const
		{
			return m_allocation;
		}

		void simulation()
		{
			for (ProcessingUnit<Data> &processingUnit : m_processingUnits)
				processingUnit.reset();

			for (size_t y = 0; y < Data::sizeY(); ++y)
				for (size_t x = 0; x < Data::sizeX(); ++x)
					m_processingUnits.at(m_allocation(x, y)).simulation(x, y);

			m_communicationDirty = true;
		}

		float maximalRunTime() const
		{
			return std::max_element(m_processingUnits.begin(), m_processingUnits.end(),
				[](const auto &lhs, const auto &rhs)
				{
					return lhs.runTime() < rhs.runTime();
				})->runTime();
		}

		float standardDeviationRunTime() const
		{
			const unsigned sum = std::accumulate(m_processingUnits.begin(), m_processingUnits.end(),
				0, [](unsigned accumulation, const ProcessingUnit<Data> &processingUnit)
					-> unsigned
				{
					return accumulation + processingUnit.runTime();
				});
			const float mean = float(sum) / m_processingUnits.size();
			const float squareDiffrenceToMean = std::accumulate(m_processingUnits.begin(),
				m_processingUnits.end(), 0.0f,
				[&mean](float accumulation, const ProcessingUnit<Data> &processingUnit)
				{
					return accumulation + std::pow(processingUnit.runTime() - mean, 2);
				});

			return std::sqrt(squareDiffrenceToMean / m_processingUnits.size());
		}

		float communication() const
		{
			if (m_communicationDirty)
			{
				m_communication = std::accumulate(m_processingUnits.begin(),
					m_processingUnits.end(), 0,
					[](unsigned accumulation, const ProcessingUnit<Data> &processingUnit)
					{
						return accumulation + processingUnit.communication();
					});

				m_communicationDirty = false;
			}

			return m_communication;
		}

		float maximalCommunication() const
		{
			if (m_communicationDirty)
			{
				m_maximalCommunication = std::max_element(m_processingUnits.cbegin(),
					m_processingUnits.cend(), [](const auto &lhs, const auto &rhs)
					{
						return lhs.communication() < rhs.communication();
					})->communication();

				m_communicationDirty = false;
			}

			return m_maximalCommunication;
		}

	// Members

	private:
		mutable bool m_communicationDirty;
		mutable float m_communication;
		mutable float m_maximalCommunication;

		std::vector<ProcessingUnit<Data>> m_processingUnits;
		Allocation<Data> m_allocation;

};
