#pragma once

#include <unordered_set>
#include <algorithm>

#include <boost/intrusive/list.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////

template <size_t t_size, size_t t_cacheLineSize>
class Cache
{
	// Types

	public:
		static const size_t cacheLineSize = t_cacheLineSize;

	private:
		static const size_t s_maximalNumberOfEntrys = t_size / cacheLineSize;

		struct Entry : public boost::intrusive::list_base_hook<>
		{
			Entry(size_t cacheLine)
				: cacheLine(cacheLine)
			{}

			size_t cacheLine;
		};

		struct hash
		{
			size_t operator()(const Entry & entry) const
			{
				return std::hash<size_t>()(entry.cacheLine);
			}
		};

		struct equal_to
		{
			bool operator()( const Entry& lhs, const Entry& rhs ) const
			{
				return std::equal_to<size_t>()(lhs.cacheLine, rhs.cacheLine);
			}
		};

		using EntrySet = std::unordered_multiset<Entry, hash, equal_to>;
		using EntryList = boost::intrusive::list<Entry>;

	// Methodes

	public:
		Cache()
		{
			static_assert(t_size >= cacheLineSize,
				"The cache needs to contain at least one cache line");
			static_assert(t_size % cacheLineSize == 0,
				"The cache size needs to be multiple of the cache line size");

			m_entrySet.reserve(s_maximalNumberOfEntrys);
		}

		bool access(const size_t address)
		{
			const auto cacheLine = address / cacheLineSize;

			auto entryIterator = m_entrySet.find(cacheLine);

			// Hit
			if (entryIterator != m_entrySet.end())
			{
				auto &entry = *entryIterator;

				m_entryList.erase(m_entryList.s_iterator_to(entry));
				m_entryList.push_front(const_cast<Entry &>(entry));

				return true;
			}

			// Miss and not full

			if (m_entrySet.size() < s_maximalNumberOfEntrys)
			{
				auto &entry = *m_entrySet.insert(Entry{cacheLine});

				m_entryList.push_front(const_cast<Entry &>(entry));

				return false;
			}

			// Miss and full
			auto &oldestEntry = m_entryList.back();
			m_entryList.pop_back();
			m_entrySet.erase(oldestEntry);

			auto &entry = *m_entrySet.insert(Entry{cacheLine});
			m_entryList.push_front(const_cast<Entry &>(entry));

			return false;
		}

		void reset()
		{
			m_entryList.clear();
			m_entrySet.clear();
		}

	// Memebers

	private:
		EntrySet m_entrySet;
		EntryList m_entryList;
};
