#include "Evolution.h"
#include "Utilis.h"

#include <thread>
#include <csignal>

////////////////////////////////////////////////////////////////////////////////////////////////////

const size_t sideLength = 10;

using Data = Matrix<unsigned, sideLength, sideLength>;

////////////////////////////////////////////////////////////////////////////////////////////////////

Evolution<Data, 1000> *g_evolution = nullptr;

////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
	std::function<void(size_t, size_t, const Data &)> fivePoint =
		[](size_t x, size_t y, const Data &input)
		{
			size_t const reach = 1;

			if (y >= reach) input(x, y - reach);
			if (x >= reach) input(x - reach, y);
			if (y < Data::sizeX() - reach) input(x, y + reach);
			if (x < Data::sizeY() - reach) input(x + reach, y);
		};

	std::function<void(size_t, size_t, const Data &, const Data &)> matrixMuliplication =
		[](size_t x, size_t y, const Data &lhs, const Data &rhs)
		{
			for (size_t i = 0; i < Data::sizeX(); ++i)
			{
				lhs(i, y);
				rhs(x, i);
			}
		};

	Costs costHPProLiantDL980G7{
		{10, 12, 17, 17, 19, 19, 19, 19},
		{12, 10, 17, 17, 19, 19, 19, 19},
		{17, 17, 10, 12, 19, 19, 19, 19},
		{17, 17, 12, 10, 19, 19, 19, 19},
		{19, 19, 19, 19, 10, 12, 17, 17},
		{19, 19, 19, 19, 12, 10, 17, 17},
		{19, 19, 19, 19, 17, 17, 10, 12},
		{19, 19, 19, 19, 17, 17, 12, 10}};

	Costs costHPProLiantDL580G9{
		{10, 14, 14, 14},
		{14, 10, 14, 14},
		{14, 14, 10, 14},
		{14, 14, 14, 10}};

	Evolution<Data, 1000> evolution(fivePoint, makeEqualPowerRatios(4), makeCosts(4));

	g_evolution = &evolution;

	// Signals

	struct sigaction printBest, stop;

	printBest.sa_handler = [](int signal)
		{
			std::cout << std::endl;

			g_evolution->printBest();
		};

	stop.sa_handler = [](int signal)
		{
			std::cout << std::endl;

			g_evolution->stop();
		};

	sigaction(SIGUSR1, &printBest, nullptr);
	sigaction(SIGINT, &stop, nullptr);

	// Run

	g_evolution->run();

	return EXIT_SUCCESS;
}
