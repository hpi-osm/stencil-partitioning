#pragma once

#include <array>
#include <iostream>

template<typename Value, size_t X, size_t Y>
class Matrix
{
	// Types

	public:
		using value_type = Value;

	// Methods

	public:

		// Class

		static constexpr size_t sizeX()
		{
			return X;
		}

		static constexpr size_t sizeY()
		{
			return Y;
		}

		// Object

		Matrix()
		:	data{}
		{}

		template <typename... Values>
		Matrix(Values... values)
		:	data{{static_cast<Value>(values)...}}
		{}

		Value & operator()(const size_t x, const size_t y)
		{
			return data[y * X + x];
		}

		virtual const Value & operator()(const size_t x, const size_t y) const
		{
			return data[y * X + x];
		}

		bool operator==(const Matrix<Value, X, Y> &rhs) const
		{
			for (size_t i = 0; i < Y * X; ++i)
				if (data[i] != rhs.data[i])
					return false;

			return true;
		}

		bool operator!=(const Matrix<Value, X, Y> &rhs) const
		{
			return !Matrix<Value, X, Y>::operator==(rhs);
		}

	// Members

	private:
		std::array<Value, Y * X> data;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Value, size_t X, size_t Y>
std::ostream & operator<<(std::ostream &ostream, const Matrix<Value, X, Y> &matrix)
{
	for (size_t y = 0; y < Y; ++y)
	{
		for (size_t x = 0; x < X; ++x)
		{
			const size_t id = matrix(x, y);

			ostream << "\033[1;3" << id % 7 + 1 << "m" << id << "\033[0m" ;

			if (x != X - 1)
				ostream << " ";
		}

		if (y != Y - 1)
			ostream << std::endl;
	}

	return ostream;
}
