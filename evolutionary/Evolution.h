#pragma once

#include "ProcessingUnits.h"
#ifndef NDEBUG
#include "Timer.h"
#endif

#include <parallel/algorithm>

#include <atomic>
#include <vector>
#include <algorithm>
#include <iostream>
#include <functional>
#include <cassert>

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Data, size_t populationSize>
class Evolution
{
	// Types

	private:
		using Individual = ProcessingUnits<Data>;
		using Population = std::vector<Individual>;

	// Methods

	public:
		template<typename Function>
		Evolution(Function function, const PowerRatios &powerRatios,
			const Costs &costs)
		:	m_bestFitnessValue(std::numeric_limits<decltype(m_bestFitnessValue)>::max())
		{
			if (std::accumulate(powerRatios.cbegin(), powerRatios.cend(), 0.0f) > 1.0f)
				throw std::domain_error("Sum of power ratios exceed 100%.");

			if (powerRatios.size() != costs.size() || std::any_of(costs.cbegin(), costs.cend(),
				[&](const auto &cost)
				{
					return costs.size() != cost.size();
				}))
				throw std::domain_error("Not all costs are given.");

			// Initialize population

			m_population.reserve(populationSize);

			for (size_t i = 0; i < populationSize; ++i)
			{
				m_population.emplace_back(function, powerRatios, costs);

				randomAllocation(m_population.at(i).allocation(), powerRatios);
			}
		}

		void run()
		{
			m_active = true;

			size_t unsuccessfulIterations = 0;
			size_t currentUnsuccessfulIterations = 0;

			while (m_active)
			{
				#ifndef NDEBUG
				m_iterationTimer.start();
				#endif

				const Individual &bestIndividual = selection();

				const unsigned fitnessValue = bestIndividual.communication();

				if (fitnessValue < m_bestFitnessValue)
				{
					unsuccessfulIterations = 0;
					currentUnsuccessfulIterations = 0;

					m_bestFitnessValue = fitnessValue;
					m_bestAllocations.clear();
					m_bestAllocations.push_back(bestIndividual.allocation());

					std::cout << "Fitness value: " << fitnessValue << std::endl;

					if (m_bestFitnessValue == 0)
						break;
				}
				else if (fitnessValue == m_bestFitnessValue
					&& std::find(m_bestAllocations.cbegin(), m_bestAllocations.cend(),
						bestIndividual.allocation()) == m_bestAllocations.cend())
				{
						m_bestAllocations.push_back(bestIndividual.allocation());
				}

				//++unsuccessfulIterations;
				//++currentUnsuccessfulIterations;

				#ifndef NDEBUG
				m_mutationTimer.start();
				#endif

				//const size_t resetLimit = Data::sizeX() + Data::sizeY()
				//	* static_cast<float>(unsuccessfulIterations) / currentUnsuccessfulIterations;

				//if (currentUnsuccessfulIterations < resetLimit)
					mutation(bestIndividual.allocation());
				/*else
				{
					#ifndef NDEBUG
					std::cout << "Reset after: " << currentUnsuccessfulIterations << " iterations"
						<< std::endl;
					#endif

					currentUnsuccessfulIterations = 0;

					mutation(m_bestAllocations.back());
				}*/

				#ifndef NDEBUG
				m_mutationTimer.stop();
				m_iterationTimer.stop();
				#endif
			}

			printBest();

			#ifndef NDEBUG
			auto printStats = [&](const std::string &name, const Timer &timer)
			{
				std::cout << "Avarage " << name << " duration: "
					<< std::chrono::nanoseconds(timer.avarageTime()).count() << " ns ("
					<< static_cast<float>(timer.avarageTime().count())
						/ m_iterationTimer.avarageTime().count() * 100 << " %)"
					<< std::endl;
			};

			printStats("simulation", m_simulationTimer);
			printStats("selection", m_selectionTimer);
			printStats("mutation", m_mutationTimer);

			std::cout << "Avarage iteration duration: "
					<< std::chrono::nanoseconds(m_iterationTimer.avarageTime()).count() << " ns"
					<< std::endl;

			#endif
		}

		void stop()
		{
			m_active = false;
		}

		void printBest()
		{
			std::cout << "Best Fitness value: " << m_bestFitnessValue << std::endl;

			for (const auto &allocation : m_bestAllocations)
				std::cout << allocation << std::endl << std::endl;
		}

		const Individual & selection()
		{
			#ifndef NDEBUG
			m_simulationTimer.start();
			#endif

			__gnu_parallel::for_each(m_population.begin(), m_population.end(),
				[](Individual &individual){individual.simulation();});

			#ifndef NDEBUG
			m_simulationTimer.stop();
			m_selectionTimer.start();
			#endif

			const auto output = __gnu_parallel::min_element(m_population.cbegin(),
				m_population.cend(), [&](const Individual &lhs, const Individual &rhs)
				{
					return lhs.communication() < rhs.communication();
				});

			#ifndef NDEBUG
			m_selectionTimer.stop();
			#endif

			return *output;
		}

		void mutation(const Allocation<Data> &parentAllocation)
		{
			static std::bernoulli_distribution elitismDistribution(0.5);
			static std::exponential_distribution<float> numberOfAdditionalChangesDistribution(0.5);

			struct Change
			{
				size_t x1, y1;
				size_t x2, y2;

				bool operator==(const Change &rhs)
				{
					return (x1 == rhs.x1 && y1 == rhs.y1 && x2 == rhs.x2 && y2 == rhs.y2)
						|| (x1 == rhs.x2 && y1 == rhs.y2 && x2 == rhs.x1 && y2 == rhs.y1);
				}
			};

			auto applyChange = [](Allocation<Data> &allocation, const Change &change)
			{
				std::swap(allocation(change.x1, change.y1), allocation(change.x2, change.y2));
			};

			auto generateChange = [&]()
			{
				Change change;

				do
				{
					change = {
						generateX(), generateY(),
						generateX(), generateY()};
				}
				while (parentAllocation(change.x1, change.y1)
					== parentAllocation(change.x2, change.y2));

				return change;
			};

			auto individualIterator = m_population.begin();

			// Elitist selection
			// https://en.wikipedia.org/wiki/Genetic_algorithm#Elitism

			//if (elitismDistribution(s_randomEngine))
			//	individualIterator++->allocation() = parentAllocation;

			__gnu_parallel::for_each(individualIterator, m_population.end(),
				[&](Individual &individual)
			{
				auto &allocation = individual.allocation();
				allocation = parentAllocation;

				const auto change = generateChange();
				applyChange(allocation, change);

				// Additional changes

				const size_t numberOfAdditionalChanges = static_cast<size_t>(
					numberOfAdditionalChangesDistribution(s_randomEngine));

				Change additionalChange;

				for (size_t i = 0; i < numberOfAdditionalChanges; ++i)
				{
					do
					{
						const auto stddevX = Data::sizeX() / individual.numberOfProcessingUnits();
						const auto stddevY = Data::sizeY() / individual.numberOfProcessingUnits();

						additionalChange = {
							generateX(change.x1, stddevX), generateX(change.y1, stddevY),
							generateX(change.x2, stddevX), generateX(change.y2, stddevY)};
					}
					while (allocation(additionalChange.x1, additionalChange.y1)
							== allocation(additionalChange.x2, additionalChange.y2)
						|| allocation(additionalChange.x1, additionalChange.y1)
							!= parentAllocation(additionalChange.x1, additionalChange.y1)
						|| allocation(additionalChange.x2, additionalChange.y2)
							!= parentAllocation(additionalChange.x2, additionalChange.y2));

					applyChange(allocation, additionalChange);
				}

				/*auto generateAdditionalChange = [&](Change &additionalChange,
					const Change &lastChange)
				{
					do
					{
						additionalChange = {
							lastChange.x2, lastChange.y2,
							generateX(), generateY()};
					}
					while (additionalChange == change
						|| allocation(additionalChange.x1, additionalChange.y1)
							== allocation(additionalChange.x2, additionalChange.y2));
				};

				std::array<Change, 2> doubleBuffer;
				Change &lastChange = doubleBuffer[0];

				lastChange = change;

				for (size_t i = 0; i < numberOfAdditionalChanges; ++i)
				{
					const Change &lastChange = doubleBuffer[i % 2];
					Change &additionalChange = doubleBuffer[(i + 1) % 2];

					generateAdditionalChange(additionalChange, lastChange);
					applyChange(allocation, additionalChange);
				}*/
			});
		}

	private:
		// Class

		static size_t generateX()
		{
			static std::uniform_int_distribution<size_t> distribution(0, Data::sizeX() - 1);

			return distribution(s_randomEngine);
		}

		static size_t generateY()
		{
			static std::uniform_int_distribution<size_t> distribution(0, Data::sizeY() - 1);

			return distribution(s_randomEngine);
		}

		static size_t generateX(const size_t input, const float standardDeviation)
		{
			std::normal_distribution<float> distribution(input, standardDeviation);
			size_t output;

			do
				output = static_cast<size_t>(distribution(s_randomEngine));
			while (output == input || output >= Data::sizeX());

			return output;
		}

		static size_t generateY(const size_t input, const float standardDeviation)
		{
			std::normal_distribution<float> distribution(input, standardDeviation);
			size_t output;

			do
				output = static_cast<size_t>(distribution(s_randomEngine));
			while (output == input || output >= Data::sizeY());

			return output;
		}

		// Object

		void randomAllocation(Allocation<Data> &allocation,	PowerRatios powerRatios) const
		{
			for (size_t id = 1; id < powerRatios.size(); ++id)
			{
				const auto &powerRatio = powerRatios.at(id);
				const size_t numberOfCells = std::round(powerRatio * Data::sizeX() * Data::sizeY());

				for (size_t i = 0; i < numberOfCells; ++i)
				{
					size_t x, y;

					do
					{
						x = generateX();
						y = generateY();
					}
					while (allocation(x, y) != 0);

					allocation(x, y) = id;
				}
			}
		}

	// Members

	private:
		// Class

		static std::random_device s_randomDevice;
		static std::mt19937 s_randomEngine;

		// Object

		std::atomic<bool> m_active;

		unsigned m_bestFitnessValue;
		std::vector<Allocation<Data>> m_bestAllocations;

		Population m_population;

		#ifndef NDEBUG
		Timer m_iterationTimer;
		Timer m_simulationTimer;
		Timer m_selectionTimer;
		Timer m_mutationTimer;
		#endif
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Data, size_t populationSize>
std::random_device Evolution<Data, populationSize>::s_randomDevice;

template<typename Data, size_t populationSize>
std::mt19937 Evolution<Data, populationSize>::s_randomEngine(
	Evolution<Data, populationSize>::s_randomDevice());
