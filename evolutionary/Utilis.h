#pragma once

#include <array>
#include <iostream>
#include <iomanip>
#include <tuple>
#include <utility>

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Type, size_t index>
using expendType = Type;

template<size_t index, typename Type>
constexpr Type expendValue(Type powerRatio)
{
	return powerRatio;
}

template<size_t... indices>
constexpr auto makePowerRatios(const float powerRatio, std::index_sequence<indices...>)
{
	return std::make_tuple<expendType<float, indices>...>(expendValue<indices>(powerRatio)...);
}

template<size_t count>
constexpr auto makePowerRatios()
{
	return makePowerRatios(1.0f / count, std::make_index_sequence<count>());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Addend, typename... Addends>
constexpr auto sum(const std::tuple<Addend, Addends...> addends, std::index_sequence<>)
{
	return Addend();
}

template<typename... Addends, size_t index, size_t... indices>
constexpr auto sum(const std::tuple<Addends...> addends, std::index_sequence<index, indices...>)
{
	return std::get<index>(addends) + sum(addends, std::index_sequence<indices...>());
}

template<typename... Addends>
constexpr auto sum(const std::tuple<Addends...> addends)
{
	return sum(addends, std::index_sequence_for<Addends...>());
}
