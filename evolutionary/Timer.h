#pragma once

#include <chrono>

class Timer
{
	// Types

	private:
		using clock = std::chrono::high_resolution_clock;

	// Methods

	public:
		Timer()
		:	m_numberOfRuns(0),
			m_duration(clock::duration::zero())
		{}

		void reset()
		{
			m_duration = clock::duration::zero();
		}

		void start()
		{
			m_start = clock::now();
		}

		void stop()
		{
			++m_numberOfRuns;
			m_duration += clock::now() - m_start;
		}

		clock::duration totalTime() const
		{
			return m_duration;
		}

		clock::duration avarageTime() const
		{
			return m_duration / m_numberOfRuns;
		}

	// Members

	private:
		clock::time_point m_start;

		unsigned m_numberOfRuns;
		clock::duration m_duration;
};
