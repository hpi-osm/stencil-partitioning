#pragma once

#include "ProcessingUnits.h"

#include <tuple>
#include <atomic>

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Data>
class StochasticOptimization
{
	// Methods

	public:
		StochasticOptimization()
		:	m_bestFitnessValue(std::numeric_limits<decltype(m_bestFitnessValue)>::max())
		{}

		virtual void run()
		{
			this->m_active = true;

			runImplementation();

			this->printBest();
		}

		void stop()
		{
			m_active = false;
		}

		void printBest()
		{
			std::cout << "Best Fitness value: " << m_bestFitnessValue << std::endl;

			for (const auto &allocation : m_bestAllocations)
				std::cout << allocation << std::endl << std::endl;
		}

	protected:
		// Class

		static size_t generateX()
		{
			static std::uniform_int_distribution<size_t> distribution(0, Data::sizeX() - 1);

			return distribution(s_randomEngine);
		}

		static size_t generateY()
		{
			static std::uniform_int_distribution<size_t> distribution(0, Data::sizeY() - 1);

			return distribution(s_randomEngine);
		}

		static size_t generateX(const size_t input, const float standardDeviation)
		{
			std::normal_distribution<float> distribution(input, standardDeviation);
			size_t output;

			do
				output = static_cast<size_t>(distribution(s_randomEngine));
			while (output == input || output >= Data::sizeX());

			return output;
		}

		static size_t generateY(const size_t input, const float standardDeviation)
		{
			std::normal_distribution<float> distribution(input, standardDeviation);
			size_t output;

			do
				output = static_cast<size_t>(distribution(s_randomEngine));
			while (output == input || output >= Data::sizeY());

			return output;
		}

		// Object

		virtual void runImplementation() = 0;

		template<typename... PowerRatios>
		void randomAllocation(Allocation<Data> &allocation,	std::tuple<PowerRatios...> powerRatios)
			const
		{
			/*const size_t numberOfProcessingUnits = sizeof...(PowerRatios);

			static std::uniform_int_distribution<size_t> distribution(0,
				numberOfProcessingUnits - 1);

			std::array<unsigned, numberOfProcessingUnits> numberOfCells;
			size_t id;

			for (size_t y = 0; y < Data::sizeY(); ++y)
				for (size_t x = 0; x < Data::sizeX(); ++x)
					do
						id = distribution(s_randomEngine);
					while (++(numberOfCells.at(id))
						> std::get<id>(powerRatios) * Data::sizeX() * Data::sizeY());*/

			randomAllocation(allocation, powerRatios, std::index_sequence_for<PowerRatios...>());
		}

		template<typename... PowerRatios, size_t id, size_t... ids>
		void randomAllocation(Allocation<Data> &allocation,
			std::tuple<PowerRatios...> powerRatios,	std::index_sequence<id, ids...>) const
		{
			for (const auto &powerRatio : powerRatios)
			const size_t numberOfCells = std::get<id>(powerRatios) * Data::sizeX() * Data::sizeY();

			for (size_t i = 0; i < numberOfCells; ++i)
			{
				size_t x, y;

				do
				{
					x = generateX();
					y = generateY();
				}
				while (allocation(x, y) != 0);

				allocation(x, y) = id;
			}

			randomAllocation(allocation, powerRatios, std::index_sequence<ids...>());
		}

		template<typename... PowerRatios>
		void randomAllocation(Allocation<Data> &, std::tuple<PowerRatios...>,
			std::index_sequence<>) const
		{}

	// Members

	protected:
		// Class

		static std::random_device s_randomDevice;
		static std::mt19937 s_randomEngine;

		// Object

		std::atomic<bool> m_active;

		unsigned m_bestFitnessValue;
		std::vector<Allocation<Data>> m_bestAllocations;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Data>
std::random_device StochasticOptimization<Data>::s_randomDevice;

template<typename Data>
std::mt19937 StochasticOptimization<Data>::s_randomEngine(StochasticOptimization<Data>::s_randomDevice());
