#pragma once

#include <vector>
#include <algorithm>
#include <tuple>
#include <functional>
#include <exception>

#include "Matrix.h"
#include "Cache.h"

template<typename Data>
using Allocation = Matrix<size_t, Data::sizeX(), Data::sizeY()>;

using Cost = std::vector<float>;

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename Data>
class ProcessingUnit
{
	// Types

	private:
		class MeasurementMatrix : public Data
		{
			// Methods

			public:
				MeasurementMatrix(const size_t id, ProcessingUnit<Data> &processingUnit)
				:	m_id(id),
					m_processingUnit(processingUnit)
				{}

				const typename Data::value_type & operator()(const size_t x, const size_t y) const
				{
					const auto inputOffset = m_id * Data::sizeX() * Data::sizeY();
					const auto offset = inputOffset + y * Data::sizeX() + x;
					const auto &otherId = m_processingUnit.m_allocation(x, y);

					const auto cacheHit = m_processingUnit.m_cache.access(
						offset * sizeof(typename Data::value_type));

					m_processingUnit.m_communication += m_processingUnit.m_cost.at(otherId)
						* (!cacheHit && otherId != m_processingUnit.m_id);

					/*for (size_t i = 0; i < 2; ++i)
					{
						const auto address =
							offset * sizeof(typename Data::value_type)
							+ i * decltype(m_cache)::cacheLineSize;

						m_processingUnit.m_cache.access(address);
					}*/

					return Data::operator()(x, y);
				}

			// Members

			private:
				const size_t m_id;

				ProcessingUnit<Data> &m_processingUnit;
		};

		using MeasurementMatrixs = std::vector<MeasurementMatrix>;

	// Methods

	public:
		template <typename... Inputs>
		ProcessingUnit(const size_t id, std::function<void(size_t, size_t, Inputs...)> function,
			const float &powerRatio, const Cost &cost, const Allocation<Data> &allocation)
		: 	m_id(id),
			m_powerRatio(powerRatio),
			m_cost(cost),
			m_inputs(createInputs(std::index_sequence_for<Inputs...>())),
			m_function(bindInputs(function, std::index_sequence_for<Inputs...>())),
			m_allocation(allocation)
		{}

		void reset()
		{
			m_runTime = 0;
			m_communication = 0;

			m_cache.reset();
		}

		void simulation(size_t x, size_t y)
		{
			m_runTime++;
			m_function(x, y);
		}

		float runTime() const
		{
			return m_runTime / m_powerRatio;
		}

		float communication() const
		{
			return m_communication;
		}

	private:
		template<size_t... ids>
		auto constexpr createInputs(std::index_sequence<ids...>)
		{
			return MeasurementMatrixs{MeasurementMatrix(ids, *this)...};
		}

		template<typename Function, size_t... ids>
		auto constexpr bindInputs(Function function, std::index_sequence<ids...>) const
		{
			return std::bind(function, std::placeholders::_1, std::placeholders::_2,
				std::ref(m_inputs.at(ids))...);
		}

	// Members

	private:
		const size_t m_id;
		const float m_powerRatio;
		const Cost m_cost;

		unsigned m_runTime{0};
		float m_communication{0};
		MeasurementMatrixs m_inputs;
		Cache<Data::sizeX() * Data::sizeY()  * sizeof(typename Data::value_type),  sizeof(typename Data::value_type)> m_cache;

		std::function<void(size_t, size_t)> m_function;
		const Allocation<Data> &m_allocation;
};
