// Use this to measure performance
#define PFM_ENABLED
// Other performance measurement tools
//#define LIKWID_ENABLED
//#define PCM_ENABLED
// Accesses at edges of the grid are performed first
//#define EDGE_FIRST
//#define DEBUG_DATA
//#define CACHE
//#define COUNTER
#define LIBNUMA

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <array>
#include <thread>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/geometries.hpp>
#include <boost/geometry/geometries/register/linestring.hpp>

#ifdef PFM_ENABLED
#include <perfmon/pfmlib_perf_event.h>
#endif


#ifdef PCM_ENABLED
#include "cpucounters.h"
#endif

#ifdef LIKWID_ENABLED
#include <likwid.h>
#endif

#include "CyclicBarrier.h"
#include "Timer.h"

#include "numamm.h"

constexpr size_t operator "" _KB(unsigned long long size)
{
   return static_cast<size_t>(size * 1024);
}

using Point = boost::geometry::model::d2::point_xy<float>;
using PointSize_t = boost::geometry::model::d2::point_xy<size_t>;
using Polygon = boost::geometry::model::polygon<Point, true, true, std::vector, std::vector, numa::LocalAllocator, numa::LocalAllocator>;
using Line = numa::localVector<Point>;
BOOST_GEOMETRY_REGISTER_LINESTRING(Line)

template <typename T>
static void memoryAccess(const T &input)
{
	T load;
	asm volatile("mov %[address], %[result]" : [result] "=r"(load) : [address] "m"(input));
}

// Configure cell size here
//const size_t cellSize = 10_KB;

//using Cell = numa::localVector<uint8_t>;//std::array<uint8_t, cellSize>;
using Data = numa::localVector<uint8_t>;//Cell>;




#ifdef PCM_ENABLED
void setupCustomCoreEvent()
{
	PCM &instance = *PCM::getInstance();

	const auto eventCodeOffcoreResponse0 = 0xB7;
	const auto eventCodeOffcoreResponse1 = 0xBB;

	const auto OffcoreResponse_DemandDataRead_RemoteNode = 0x1FFF800001;

	std::array<EventSelectRegister, 4> eventRegisters;

	EventSelectRegister eventRegister;
	eventRegister.value = 0;
	eventRegister.fields.usr = 1;
	eventRegister.fields.os = 0;
	eventRegister.fields.enable = 1;
	eventRegister.fields.any_thread = 1;

	eventRegisters.fill(eventRegister);

	eventRegisters[0].fields.event_select = eventCodeOffcoreResponse0;
	eventRegisters[0].fields.umask = 0x01;
	eventRegisters[1].fields.event_select = eventCodeOffcoreResponse1;
	eventRegisters[1].fields.umask = 0x01;

	PCM::ExtendedCustomCoreEventDescription config;
    config.fixedCfg = nullptr; // default
    config.nGPCounters = 4;

    if (instance.getCPUModel() != PCM::HASWELLX)
    {
    	std::cerr << "Your processor is not supported." << std::endl;
        exit(EXIT_FAILURE);
    }

    config.OffcoreResponseMsrValue[0] = OffcoreResponse_DemandDataRead_RemoteNode;
    config.OffcoreResponseMsrValue[1] = OffcoreResponse_DemandDataRead_RemoteNode;

    config.gpCounterCfg = eventRegisters.data();

    PCM::ErrorCode status = instance.program(PCM::EXT_CUSTOM_CORE_EVENTS, &config);

    switch (status)
    {
        case PCM::Success:
            break;
        case PCM::MSRAccessDenied:
            std::cerr << "Access to Intel(r) Performance Counter Monitor has denied (no MSR or PCI CFG space access)." << std::endl;
            exit(EXIT_FAILURE);
        case PCM::PMUBusy:
            std::cerr << "Access to Intel(r) Performance Counter Monitor has denied (Performance Monitoring Unit is occupied by other application). Try to stop the application that uses PMU." << std::endl;
            std::cerr << "Alternatively you can try to reset PMU configuration at your own risk. Try to reset? (y/n)" << std::endl;
            char yn;
            std::cin >> yn;
            if ('y' == yn)
            {
                instance.resetPMU();
               	std::cerr << "PMU configuration has been reset. Try to rerun the program again." << std::endl;
            }
            exit(EXIT_FAILURE);
        default:
            std::cerr << "Access to Intel(r) Performance Counter Monitor has denied (Unknown error)." << std::endl;
            exit(EXIT_FAILURE);
    }
}
#endif

auto parseCSV(const std::string &path)
{
	numa::localVector<Polygon> polygons;

	std::ifstream csv;
	csv.open(path);

	std::string line;

	Polygon polygon;

	while (std::getline(csv, line))
	{
		if (line.empty())
		{
			polygons.push_back(polygon);
			boost::geometry::clear(polygon);

			continue;
		}

		std::stringstream lineStream(line);

		float x, y;
		lineStream >> x;
		lineStream >> y;

		boost::geometry::append(polygon, Point(std::move(x) , std::move(y)));
	}

	return polygons;
}

int main(int argc, char *argv[])
{
	numa::bindCpuToNode(0);

	#ifdef PFM_ENABLED
	struct perf_event_attr attribute{};
	int returnValue;

	returnValue = pfm_initialize();

	if (returnValue != PFM_SUCCESS)
		std::cerr << "ERROR: cannot initialize library: " <<  pfm_strerror(returnValue) << std::endl;

	// Remote access on remote DRAM or remote cache
	auto eventRemoteNode = "OFFCORE_RESPONSE_0"
		":DMND_DATA_RD:L3_MISS_REMOTE_HOP0:L3_MISS_REMOTE_HOP1:L3_MISS_REMOTE_HOP2P"
		":SNP_NONE:SNP_NOT_NEEDED:SNP_MISS:SNP_NO_FWD:SNP_FWD:SNP_HITM";
	// Remote access on remote cache only
	auto eventRemoteDram = "OFFCORE_RESPONSE_0"
		":DMND_DATA_RD:L3_MISS_REMOTE_HOP0:L3_MISS_REMOTE_HOP1:L3_MISS_REMOTE_HOP2P"
		":SNP_NOT_NEEDED:SNP_MISS:SNP_NO_FWD";
	auto eventMemLoadRemoteDram = "MEM_LOAD_UOPS_L3_MISS_RETIRED:REMOTE_DRAM";
	// Measure on user level
	returnValue = pfm_get_perf_event_encoding(
		eventMemLoadRemoteDram, PFM_PLM1|PFM_PLM2|PFM_PLM3, &attribute, NULL, NULL);

	if (returnValue != PFM_SUCCESS)
		std::cerr << "ERROR: Cannot find encoding: " << pfm_strerror(returnValue) << std::endl;

	attribute.disabled = 1;
	#endif

	#ifdef LIKWID_ENABLED
	LIKWID_MARKER_INIT;
	#endif

	#ifdef PCM_ENABLED
	setupCustomCoreEvent();
	#endif

	if (argc != 7)
		return EXIT_FAILURE;

	const auto polygons = parseCSV(argv[1]);

	size_t columns(std::stoi(argv[2]));
	size_t rows(std::stoi(argv[3]));

	const size_t cellSize = std::stoi(argv[4]);
	const size_t iterations = std::stoi(argv[5]);

	const size_t reach = std::stoi(argv[6]);

	const size_t dataSize = columns * rows;

	auto cellAccess = [&](const uint8_t &cell)
	{
		//std::for_each(cell.cbegin(), cell.cend(),
		//	[](const auto &entry){memoryAccess(entry);});

		for (unsigned i = 0; i < cellSize; ++i)
			memoryAccess((&cell)[i]);
	};

	numa::localVector<uint8_t> map(dataSize);
	numa::localVector<numa::localVector<PointSize_t>> workLists(polygons.size());

	Line line;
	line.reserve(2);

	numa::localVector<Point> intersections;
	intersections.reserve(2);

	auto scanLine = [&](const size_t &row)
	{
		const auto step = row / (rows - 1.0);
		const auto rowWidth = 1.0 / rows;
		const auto margin = rowWidth / 2.0;
		const auto scaling =  1.0 - rowWidth;

		const auto y = step * scaling + margin;
		const auto yTransformed = y * 2.0 - 1.0;

		assert(yTransformed >= -1 && yTransformed <= 1);

		return Line{
			Point(-10, yTransformed),
			Point(10, yTransformed)};
	};

	auto calculateColumn = [&](const auto &x)
	{
		const auto xTransformed = (x + 1.0) / 2.0;

		return std::round(xTransformed * columns);
	};

	#ifdef EDGE_FIRST
	for (size_t row = 0; row < rows; ++row)
	{
		line = scanLine(row);

		for (size_t i = 0; i < polygons.size(); ++i)
		{
			const auto &polygon = polygons[i];

			intersections.clear();

			boost::geometry::intersection(polygon, line, intersections);

			assert(intersections.size() % 2 == 0);

			std::sort(intersections.begin(), intersections.end(),
				[](const auto &intersection1, const auto &intersection2)
				{
					return intersection1.x() <= intersection2.x();
				});

			for (size_t j = 0; j < intersections.size(); j += 2)
			{
				const auto &intersection1 = intersections.at(j);
				const auto &intersection2 = intersections.at(j + 1);

				const std::array<int, 2> intersectionColumns{
					static_cast<int>(calculateColumn(intersection1.x())),
					static_cast<int>(calculateColumn(intersection2.x()) - 1)};

				if (intersectionColumns[0] > intersectionColumns[1])
					continue;

				if (intersectionColumns[0] == intersectionColumns[1])
				{
					map[row * columns + intersectionColumns[0]] = i;
					workLists[i].push_back({static_cast<size_t>(intersectionColumns[0]), row});

					continue;
				}

				for (const auto &col : intersectionColumns)
				{
					map[row * columns + col] = i;

					//if (col != 0 && row != 0 && col != columns - 1 && row != rows - 1)
						workLists[i].push_back({static_cast<size_t>(col), row});
				}
			}
		}
	}
	#endif

	// Rasterize grid
	for (size_t row = 0; row < rows; ++row)
	{
		line = scanLine(row);

		for (size_t id = 0; id < polygons.size(); ++id)
		{
			const auto &polygon = polygons[id];

			intersections.clear();
			boost::geometry::intersection(polygon, line, intersections);

			assert(intersections.size() % 2 == 0);

			std::sort(intersections.begin(), intersections.end(),
				[](const auto &intersection1, const auto &intersection2)
				{
					return intersection1.x() <= intersection2.x();
				});

			for (size_t j = 0; j < intersections.size(); j += 2)
			{
				const auto &intersection1 = intersections.at(j);
				const auto &intersection2 = intersections.at(j + 1);

				#ifndef EDGE_FIRST
				const size_t start = calculateColumn(intersection1.x());
				const size_t end = calculateColumn(intersection2.x());
				#else
				const int start = calculateColumn(intersection1.x()) + 1;
				const int end = calculateColumn(intersection2.x()) - 1;
				#endif

				for (auto col = start; col < end ; ++col)
				{
					assert(col >= 0 && col < columns);

					map[row * columns + col] = id;

					//if (col != 0 && row != 0 && col != columns - 1 && row != rows - 1)
						workLists[id].push_back({static_cast<size_t>(col), row});
				}
			}
		}
	}

	#ifdef DEBUG_DATA
	for (size_t row = 0; row < rows; ++row)
	{
		for (size_t col = 0; col < columns; ++col)
			std::cout << static_cast<int>(map[row * columns + col]) << "\t";

		std::cout << std::endl;
	}

	std::cout << std::endl;

	for (const auto &workList : workLists)
	{
		for (const auto &point : workList)
			std::cout << "(" << point.x() << ", " << point.y() << ") ";

		std::cout << std::endl << std::endl;
	}

	std::cout << std::endl;

	for (const auto &workList : workLists)
		std::cout << workList.size() << std::endl;

	std::cout << std::accumulate(workLists.cbegin(), workLists.cend(), 0,
		[](auto i, const auto &point)
		{
			return i + point.size();
		}) << std::endl;
	#endif

	numa::localVector<Data **> lookUp(workLists.size());

	CyclicBarrier barrier(workLists.size());

	#ifdef COUNTER
	numa::localVector<unsigned> remoteAccessCounters(workLists.size());
	numa::localVector<unsigned> localAccessCounters(workLists.size());
	numa::localVector<unsigned> cacheCounters(workLists.size());
	#endif

	#if defined(PCM_ENABLED) || defined(PFM_ENABLED)
	numa::localVector<unsigned long long> counter(polygons.size(), 0);
	#endif

	// Execute threads with given partitioning
	auto work = [&](const size_t id)
	{
		// Bind execution to given NUMA node ID
		numa::bindCpuToNode(id);
		//numa::bindMemoryToNode(id);

		#ifdef PFM_ENABLED
		int fileDescriptor = perf_event_open(&attribute, 0, -1, -1, 0);
		#endif

		#ifdef LIKWID_ENABLED
		LIKWID_MARKER_THREADINIT;
		#endif

		#ifdef COUNTER
		unsigned remoteAccessCounter = 0;
		unsigned localAccessCounter = 0;
		unsigned cacheCounter = 0;
		#endif

		// Data has a custom NUMA allocator
		// Double-buffered array of grid data that is swapped to allow for consistent accesses to data from last iteration
		std::array<Data, 2> localData{{Data(dataSize * cellSize, 0), Data(dataSize * cellSize)}};
		auto *inputData = &localData[0];
		auto *outputData = &localData[1];

		// Thread registers its input pointer to the global look-up table
		lookUp[id] = &inputData;

		// Sync threads
		barrier.wait();

		// Create local copies to avoid remote accesses
		auto localLookUp = lookUp;
		auto workList = workLists[id];
		auto localMap = map;

		#ifdef CACHE
		numa::localVector<bool> cacheValid(inputData->size(), false);
		Data cache(inputData->size());
		#endif

		// Input: location in the grid
		// Looks for the thread owning the location
		// Finds out
		auto access = [&](const auto location)
		{
			const auto offset = location.y() * columns + location.x();

			const auto &otherId = localMap.at(offset);
			const auto &remoteData = **localLookUp.at(otherId);
			const auto &cell = remoteData.at(offset * cellSize);

			#ifdef CACHE
			if (id != otherId)
			{
				if (cacheValid.at(offset))
				{
					const auto &cell = cache.at(offset);

					cellAccess(cell);

					#ifdef COUNTER
					cacheCounter++;
					#endif

					return;
				}

				cache.at(offset) = cell;
				cacheValid.at(offset) = true;
			}
			#endif

			assert(id != numa::nodeOfCurrentThread()
				|| otherId != numa::nodeOfMemory(
					const_cast<void *>(static_cast<const void *>(&cell)), sizeof(Cell)));

			if (otherId != id)
				cellAccess(cell);

			#ifdef COUNTER
			if (otherId != id)
				remoteAccessCounter++;
			else
				localAccessCounter++;
			#endif
		};

		for (size_t i = 1; i <= iterations; ++i)
		{
			auto accessPattern = [&](const auto &point)
			{
				//const size_t reach = 1;

				for (size_t j = 1; j <= reach; ++j)
				{
					if (point.y() >= j)
						access(PointSize_t(point.x(), point.y() - j));

					if (point.x() >= j)
						access(PointSize_t(point.x() - j, point.y()));

					if (point.y() < rows - j)
						access(PointSize_t(point.x(), point.y() + j));

					if (point.x() < columns - j)
						access(PointSize_t(point.x() + j, point.y()));
				}

				//auto &output = (*outputData).at(point.x() + point.y() * columns);
				//std::fill(std::begin(output), std::end(output), id * i);
				//for (int j = 0; j < cellSize; ++j)
				//	(&output)[j] = id * i;
			};

			#ifdef PCM_ENABLED
			const auto beforeState = getCoreCounterState(numa::currentCore());
			#endif

			#ifdef LIKWID_ENABLED
			LIKWID_MARKER_START("access");
			#endif

			// Start measurement
			#ifdef PFM_ENABLED
			ioctl(fileDescriptor, PERF_EVENT_IOC_ENABLE, 0);
			#endif

			for (const auto &point : workList)
				accessPattern(point);

			// Stop measurement
			#ifdef PFM_ENABLED
			ioctl(fileDescriptor, PERF_EVENT_IOC_DISABLE, 0);

			read(fileDescriptor, &counter[id], sizeof(decltype(counter[id])));
			#endif

			#ifdef LIKWID_ENABLED
			LIKWID_MARKER_STOP("access");
			#endif

			#ifdef PCM_ENABLED
			const auto afterState = getCoreCounterState(numa::currentCore());

			counter[id] += getNumberOfCustomEvents(0, beforeState, afterState);
			#endif

			// Sync with other threads
			barrier.wait();

			#ifdef CACHE
			cacheValid = decltype(cacheValid)(cacheValid.size(), false);
			#endif

			// Exchange buffers
			std::swap(inputData, outputData);

			// Sync with other threads
			barrier.wait();
		}

		#ifdef COUNTER
		remoteAccessCounters[id] = remoteAccessCounter;
		localAccessCounters[id] = localAccessCounter;
		cacheCounters[id] = cacheCounter;
		#endif
	};

	numa::localVector<std::thread> workers;
	workers.reserve(workLists.size());

	for (size_t id = 0; id < workLists.size(); ++id)
		workers.push_back(std::thread(work, id));

	for (auto &worker : workers)
		worker.join();

	#if defined(PCM_ENABLED) || defined(PFM_ENABLED)
	//std::cout << std::accumulate(counter.begin(), counter.end(), 0) << std::endl;

	for (const auto &core : counter)
		std::cout << core << "\t";//std::endl;

	std::cout << std::endl;
	#endif

	#ifdef COUNTER
	std::cout << "Remote access: " << std::accumulate(remoteAccessCounters.begin(), remoteAccessCounters.end(), 0) << std::endl;
	std::cout << "Local access: " << std::accumulate(localAccessCounters.begin(), localAccessCounters.end(), 0) << std::endl;
	#ifdef CACHE
	std::cout << "Cache access: " << std::accumulate(cacheCounters.begin(), cacheCounters.end(), 0) << std::endl;
	#endif
	#endif

	#ifdef LIKWID_ENABLED
	LIKWID_MARKER_CLOSE;
	#endif

	return EXIT_SUCCESS;
}
