#! /bin/bash

referenzAllocation="../../model_4_reference.csv"
newAllocation="../../model_4_optimal.csv"

function kiloByte
{
	echo $(($1 * 1024))
}

function executeSquare
{
	model="$(basename $1 .csv)"
	fileName="$6_$model""_$2_$3_$4_$5.csv"

	./run "$1" $2 $2 $3 $4 $5 >> $fileName
}

function variableDimension
{
	bytes=10000
	#$(kiloByte 10)

	executeSquare "$1" $2 $bytes 1 1 "variableDimension"
}

function variableCellSize
{
	bytes=$2
	#$(kiloByte $2)

	executeSquare "$1" 1000 $bytes 1 1 "variableCellSize"
}

function variableIterations
{
	bytes=$(kiloByte 4)

	executeSquare "$1" 1000 $bytes $2 1 "variableIterations"
}

function variableReach
{
	bytes=5000
	#$(kiloByte 4)

	executeSquare "$1" 1000 $bytes 1 $2 "variableReach"
}

for i in {1..10}
do
	echo "Starting iteration $i"

	for value in {1..10..1}
	do
		echo "	Running Value $value"

		#variableDimension "$referenzAllocation" $(($value * 10))
		#variableDimension "$newAllocation" $(($value * 10))
		#variableDimension "$referenzAllocation" $(($value * 100))
		#variableDimension "$newAllocation" $(($value * 100))

		variableCellSize "$referenzAllocation" $((value * 100))
		variableCellSize "$newAllocation" $((value * 100))
		#variableCellSize "$referenzAllocation" $((value * 1000))
		#variableCellSize "$newAllocation" $((value * 1000))

		#variableIterations "$referenzAllocation" $value
		#variableIterations "$newAllocation" $value

		#variableReach "$referenzAllocation" $value
		#variableReach "$newAllocation" $value
	done
done
