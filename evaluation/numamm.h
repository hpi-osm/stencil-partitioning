#include <thread>
#include <vector>

#ifndef LIBNUMA

#include <hwloc.h>

class numa
{
	private:
		static hwloc_topology_t topology;

		class Initializer
		{
			public:
				Initializer()
				{
					hwloc_topology_init(&topology);
					hwloc_topology_load(topology);
				}
		};

		static Initializer initializer;

	public:
		static size_t numberOfNodes()
		{
			return hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_NODE);
		}

		static size_t currentCore()
		{
			hwloc_cpuset_t bind = hwloc_bitmap_alloc();
			hwloc_get_cpubind(topology, bind, HWLOC_CPUBIND_THREAD);
			hwloc_obj_t core = hwloc_get_next_obj_covering_cpuset_by_type(topology, bind, HWLOC_OBJ_CORE, NULL);

			return core->logical_index;
		}

		static size_t currentNode()
		{
			hwloc_cpuset_t bind = hwloc_bitmap_alloc();
			hwloc_get_cpubind(topology, bind, HWLOC_CPUBIND_THREAD);
			hwloc_obj_t node = hwloc_get_next_obj_covering_cpuset_by_type(topology, bind, HWLOC_OBJ_NODE, NULL);

			return node->logical_index;
		}

		static void bindCpuToNode(const size_t &id)
		{
			hwloc_obj_t node = hwloc_get_obj_by_type(topology, HWLOC_OBJ_NODE, id);
			//auto n = hwloc_get_nbobjs_inside_cpuset_by_type(topology, node->cpuset, HWLOC_OBJ_PU);
			//hwloc_obj_t pu = hwloc_get_obj_inside_cpuset_by_type(topology, node->cpuset, HWLOC_OBJ_PU, n - 1);

			if (hwloc_set_cpubind(topology, node->cpuset, HWLOC_CPUBIND_THREAD | HWLOC_CPUBIND_STRICT) < 0)
				std::cerr << "ERROR: CPU bind failed" << std::endl;
		}

		static void bindMemoryToNode(const size_t &id)
		{
			hwloc_obj_t node = hwloc_get_obj_by_type(topology, HWLOC_OBJ_NODE, id);

			if (hwloc_set_membind_nodeset(topology, node->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_THREAD | HWLOC_MEMBIND_STRICT) < 0)
				std::cerr << "ERROR: Mem bind failed" << std::endl;
		}

		static void * allocateOnNode(const size_t &size, const size_t &id)
		{
			hwloc_obj_t node = hwloc_get_obj_by_type(topology, HWLOC_OBJ_NODE, id);

			auto output = hwloc_alloc_membind_nodeset(topology, size, node->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_THREAD | HWLOC_MEMBIND_STRICT);

			if (output < 0)
				std::cerr << "ERROR: Mem bind failed" << std::endl;

			return std::move(output);
		}

		static void * allocateLocal(const size_t &size)
		{
			hwloc_cpuset_t bind = hwloc_bitmap_alloc();
			hwloc_get_cpubind(topology, bind, HWLOC_CPUBIND_THREAD);
			hwloc_obj_t node = hwloc_get_next_obj_covering_cpuset_by_type(topology, bind, HWLOC_OBJ_NODE, NULL);

			auto output = hwloc_alloc_membind_nodeset(topology, size, node->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_THREAD | HWLOC_MEMBIND_STRICT);

			if (output < 0)
				std::cerr << "ERROR: Mem bind failed" << std::endl;

			return std::move(output);
		}

		static size_t nodeOfMemory(void *address, size_t size)
		{
			hwloc_cpuset_t memorySet = hwloc_bitmap_alloc();
			hwloc_membind_policy_t policy;
			hwloc_get_area_membind(topology, address, size, memorySet, &policy, HWLOC_MEMBIND_THREAD);
			hwloc_obj_t memoryNode = hwloc_get_next_obj_covering_cpuset_by_type(topology, memorySet,
				HWLOC_OBJ_NODE, NULL);

			return memoryNode->logical_index;
		}

		static size_t nodeOfCurrentThread()
		{
			hwloc_cpuset_t cpuSet = hwloc_bitmap_alloc();
			hwloc_get_cpubind(topology, cpuSet, HWLOC_CPUBIND_THREAD);
			hwloc_obj_t cpuNode = hwloc_get_next_obj_covering_cpuset_by_type(topology, cpuSet,
				HWLOC_OBJ_NODE, NULL);

			return cpuNode->logical_index;
		}

		static void printBindings(const void *address, size_t size)
		{
			hwloc_cpuset_t cpuSet = hwloc_bitmap_alloc();
			hwloc_get_cpubind(topology, cpuSet, HWLOC_CPUBIND_THREAD);
			hwloc_obj_t cpuNode = hwloc_get_next_obj_covering_cpuset_by_type(topology, cpuSet,
				HWLOC_OBJ_NODE, NULL);

			hwloc_cpuset_t memorySet = hwloc_bitmap_alloc();
			hwloc_membind_policy_t policy;
			hwloc_get_area_membind(topology, address, size, memorySet, &policy, HWLOC_MEMBIND_THREAD);
			hwloc_obj_t memoryNode = hwloc_get_next_obj_covering_cpuset_by_type(topology, memorySet,
				HWLOC_OBJ_NODE, NULL);

			std::cout << "Node: " << cpuNode->logical_index
				<< " Memory: " << memoryNode->logical_index << std::endl;
		}

		template <typename T>
		struct LocalAllocator
		{
			using value_type = T;

			LocalAllocator() noexcept = default;
			template <class U>
			LocalAllocator(const LocalAllocator<U> &) noexcept
			{}

			T* allocate(std::size_t n)
			{
				return static_cast<T*>(
					allocateLocal(n * sizeof(T)));
			}

			void deallocate(T* p, std::size_t n)
			{
				hwloc_free(topology, static_cast<void *>(p), n);
			}

			template <typename U>
			constexpr bool operator ==(
				const numa::LocalAllocator<U> &rhs) noexcept
			{
				return true;
			}

			template <typename U>
			constexpr bool operator !=(
				const numa::LocalAllocator<U> &rhs) noexcept
			{
				return !(*this == rhs);
			}
		};

		template <typename T>
		using localVector = std::vector<T, LocalAllocator<T>>;
};

hwloc_topology_t numa::topology = hwloc_topology_t();
numa::Initializer numa::initializer;
#else

#include <numa.h>
#include <numaif.h>
#include <vector>

namespace numa
{
	size_t numberOfNodes()
	{
		return numa_max_possible_node();
	}

	static size_t currentCore()
	{
		return sched_getcpu();
	}

	void bindCpuToNode(const size_t &id)
	{
		numa_run_on_node(id);
	}

	void bindMemoryToNode(const size_t &id)
	{
		numa_set_preferred(id);
		numa_set_strict(true);
	}

	size_t nodeOfMemory(void *address, size_t size)
	{
		int numaNode;

		if (get_mempolicy(&numaNode, nullptr, 0, address, MPOL_F_NODE | MPOL_F_ADDR) < 0)
			std::cerr << "ERROR: get_mempolicy failed" << std::endl;

		return numaNode;
	}

	size_t nodeOfCurrentThread()
	{
		return numa_node_of_cpu(sched_getcpu());
	}

	void printBindings(void *address, size_t size)
	{
		int numaNode;

		if (get_mempolicy(&numaNode, nullptr, 0, address, MPOL_F_NODE | MPOL_F_ADDR) < 0)
			std::cerr << "ERROR: get_mempolicy failed" << std::endl;

		std::cout << "Node: " << numa_node_of_cpu(sched_getcpu())
				<< " Memory: " << numaNode << std::endl;
	}

	template <typename T>
	struct LocalAllocator
	{
		using value_type = T;

		LocalAllocator() noexcept = default;
		template <class U>
		LocalAllocator(const LocalAllocator<U> &) noexcept
		{}

		T* allocate(std::size_t n)
		{
			return static_cast<T*>(
				numa_alloc_local(n * sizeof(T)));
		}

		void deallocate(T* p, std::size_t n)
		{
			numa_free(static_cast<void *>(p), n);
		}

		template <typename U>
		constexpr bool operator ==(
			const numa::LocalAllocator<U> &rhs) noexcept
		{
			return true;
		}

		template <typename U>
		constexpr bool operator !=(
			const numa::LocalAllocator<U> &rhs) noexcept
		{
			return !(*this == rhs);
		}
	};

	template <typename T>
	using localVector = std::vector<T, LocalAllocator<T>>;
}
#endif
