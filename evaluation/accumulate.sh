#! /bin/bash

function calculateRatio
{
	values=()
	i=0

	while IFS=$'\t' read -r -a referenceCosts && IFS=$'\t'  read -r -a newCosts <&3
	do
		i=$((i + 1))

		referenceCost=$(IFS="+";bc<<<"${referenceCosts[*]}")
		newCost=$(IFS="+";bc<<<"${newCosts[*]}")

		values+=($(bc<<<"scale=10; $newCost / $referenceCost * 100"))
	done < $1 3<$2

	s=$(IFS="+";bc<<<"${values[*]}")
	mean=$(bc<<<"scale=10; $s / $i")

	v=0
	for value in ${values[@]}
	do
		v=$(bc<<<"scale=10; $v + ($value - $mean)^2")
	done

	stddev=$(bc<<<"scale=10; sqrt($v / $i)")
	echo "$3	$(printf %.2f $mean)	$(printf %.2f $stddev)"
}

function accumulate
{
	variables=()

	files=(./$1_model_4_optimal*)
	preamble=$(sed -r "s/\.\/$1_model_4_optimal_(([0-9]*_){$2}).*/\1/" <<< ${files[0]})
	epilogue=$(sed -r "s/\.\/$1_model_4_optimal_([0-9]*_){$2}[0-9]*(.*)/\2/" <<< ${files[0]})

	for file in ${files[@]}
	do
		variables+=($(sed -r "s/\.\/$1_model_4_optimal_([0-9]*_){$2}([0-9]*).*/\2/" <<< $file))
	done

	IFS=$'\n' variables=($(sort -g <<<"${variables[*]}"))

	#filename="$1_$2_${variables[0]}_to_${variables[-1]}$3"

	for variable in ${variables[@]}
	do
		echo "$variable	$(calculateRatio ./$1_model_4_reference_$preamble$variable$epilogue ./$1_model_4_optimal_$preamble$variable$epilogue)"
	done
}

function meanColumns
{
	echo -n $(awk '{total += $1} END {print total/NR}' "$1")"	"
	echo -n $(awk '{total += $2} END {print total/NR}' "$1")"	"
	echo -n $(awk '{total += $3} END {print total/NR}' "$1")"	"
	echo $(awk '{total += $4} END {print total/NR}' "$1")"	"
}

#echo "variableDimension"
#accumulate "variableDimension" 0
echo "variableCellSize"
accumulate "variableCellSize" 1
#echo "variableIterations"
#accumulate "variableIterations" 2
#echo "meanReach"
#accumulate "variableReach" 3
#echo "meanColumns"
#meanColumns variableIterations_model_4_optimal_1000_4096_1_1.csv
#meanColumns variableIterations_model_4_reference_1000_4096_1_1.csv
