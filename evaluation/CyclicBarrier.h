#include <mutex>
#include <condition_variable>

class CyclicBarrier
{
	public:
		CyclicBarrier(std::size_t threads)
			: threads{threads}, counts{}
		{}

		void wait()
		{
		    std::unique_lock<std::mutex> lock{mutex};

			auto &myCount = counts[currentCount];

		    if (++myCount < threads)
		        conditionVariable.wait(lock, [&]{return myCount >= threads;});
		    else
		    {
		    	currentCount ^= 1;
		    	counts[currentCount] = 0;

		    	conditionVariable.notify_all();
		    }
		}

	private:
		std::mutex mutex;
		std::condition_variable conditionVariable;

		std::size_t threads;
		std::array<std::size_t, 2> counts;
		std::size_t currentCount{};
};
